package com.shop.istyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingiStyleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingiStyleApplication.class, args);
	}

}
