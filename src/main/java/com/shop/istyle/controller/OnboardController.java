package com.shop.istyle.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/onboard")
public class OnboardController 
{

 @GetMapping(path="/welcome", produces = "application/json")
    public String sayWelcome() 
    {
        return "Welcome to iStyle Shoppers";
    }
	
}